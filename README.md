# README #

* This REPO is used for the casting of a new crew member for the frontend development of our App.  

### What is this repository for? ###

* Your task is to create an APP client or frontend for random generated Simpson's quotes for the Simpsons-Quote API found here https://thesimpsonsquoteapi.glitch.me/

### How do I get set up? ###

* Create a branch with your name or alias. Work only within your branch!
* App-Developers: Create an android app (apk) or iOS App using REACT NATIVE (https://facebook.github.io/react-native/docs/getting-started)
* Fullstack Developers: Create a solution according to the specification given to you.
* Include some tests in your development.


### Contribution guidelines ###

* Comment your code
* Try to think of user experience
* Use 3rd Party libraries where applicable 
* Use component level state
* Try to think of best practice for coding 
* Be creative 


### Who do I talk to? ###

* In case of any questions contact your fellow AlphaCrew members at: alpha-crew@hanseaticbank.de